const express = require("express");
const fs = require("fs");

// const luasSegitiga = require("./segitiga.js");
// console.log(luasSegitiga(3, 4));

// const isi = fs.readFileSync("./text.txt", "utf-8");
// console.log(isi);

// fs.writeFileSync("./test.txt", "l Love You 3000");

const student = require("./student.json");
console.log(student);

const app = express();

app.get("/", (req, res) => {
  res.render("index.ejs");
});

app.get("/item", (req, res) => {
  res.render("item.ejs", {
    data: student,
  });
});

app.listen(3000);
