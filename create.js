const fs = require("fs");

const createStudent = function (student) {
  fs.writeFileSync("./student.json", JSON.stringify(student));
  return student;
};

const Student = createStudent([
  {
    name: "Sabrina",
    age: 22,
    address: "BSD",
  },
  {
    name: "Tito Anggoro",
    age: 21,
    address: "Sidoarjo",
  },
]);
